package com.igtecsolucoestecnologicas.admob.presenter

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.igtecsolucoestecnologicas.admob.interactor.AdInteractor
import com.igtecsolucoestecnologicas.admob.repository.AdType

class MainPresenter(
    private val view: View?,
    private val ad: AdInteractor
) {

    interface View {
        fun showBanner(adRequest: AdRequest)
        fun adWatched()
    }

    fun showBanners() {
        view?.showBanner(
            ad.requestBannerAd()
        )
    }

    fun onLoadPubs() {
        //ad.loadAd(AdType.REWARD_INTERSTITIAL, successWatched = { this.adSuccessWatched() })
        //ad.loadAd(AdType.REWARD, successWatched = { this.adSuccessWatched() })
        ad.loadAd(AdType.INTERSTITIAL)
    }

    fun showPub() { ad.showAd() }

    private fun adSuccessWatched() {
        /** TODO zera o contador de músicas tocadas, counterPlays = 0 */
        view?.adWatched()
    }
}