package com.igtecsolucoestecnologicas.admob.repository

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback
import com.igtecsolucoestecnologicas.admob.interactor.AdInteractor

enum class AdType{
    BANNER,
    INTERSTITIAL,
    REWARD_INTERSTITIAL,
    REWARD,
}

class AdRepositoryFake(private val activity: AppCompatActivity) : AdInteractor {

    private lateinit var mType: AdType
    private var mSuccessLoad: (() -> Unit)? = null
    private var mFailedLoad: (() -> Unit)?  = null
    private var mSuccessWatched: (() -> Unit)? = null
    private var mFailedWatched: (() -> Unit)?  = null

    private var mInterstitialAd: InterstitialAd? = null
    private var rewardedInterstitialAd: RewardedInterstitialAd? = null
    private var mRewardedAd: RewardedAd? = null

    private var idsPub = mapOf(
        AdType.INTERSTITIAL to "ca-app-pub-3940256099942544/1033173712",
        AdType.REWARD_INTERSTITIAL to "ca-app-pub-3940256099942544/5354046379",
        AdType.BANNER to "ca-app-pub-3940256099942544/6300978111",
        AdType.REWARD to "ca-app-pub-3940256099942544/5224354917",
    )

    private companion object {
        const val TAG = "ADS_FAKE"
    }

    /***
     * Requisição de Ad para o banner
     */
    override fun requestBannerAd(): AdRequest = this.requestAd()

    /**
     *  Faz a requisição prévia de um anúncio dinâmico, para que possa ser exibido posteriormente
     * */
    override fun loadAd(
        type: AdType,
        successLoad: (() -> Unit)?,
        failedLoad: (() -> Unit)?,
        successWatched: (() -> Unit)?,
        failedWatched: (() -> Unit)?
    ) {
        this.mType = type
        this.mSuccessLoad = successLoad
        this.mFailedLoad = failedLoad
        this.mSuccessWatched = successWatched
        this.mFailedWatched = failedWatched

        when (type) {
            AdType.BANNER -> {}
            AdType.INTERSTITIAL -> this.loadInterstitialAd()
            AdType.REWARD_INTERSTITIAL -> this.loadRewardInterstitialAd()
            AdType.REWARD -> this.loadRewardAd()
        }
    }

    /**
     * Verifica o tipo de anúncio carregado e invoca a função de exibição
     * */
    override fun showAd() {
        when (this.mType) {
            AdType.BANNER -> {}
            AdType.INTERSTITIAL -> this.showInterstitialAd()
            AdType.REWARD_INTERSTITIAL -> this.showRewardInterstitialAd()
            AdType.REWARD -> this.showRewardAd()
        }
    }

    //region load ads
    private fun loadInterstitialAd() {
        idsPub[AdType.INTERSTITIAL]?.let {
            InterstitialAd.load(activity.applicationContext, it, this.requestAd(), object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d(TAG, adError.toString())
                    mInterstitialAd = null
                    mFailedLoad?.invoke()
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d(TAG, "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                    mSuccessLoad?.invoke()
                    this@AdRepositoryFake.setInterstitialFullScreenCallBack()
                }
            })
        }
    }

    private fun loadRewardInterstitialAd() {
        idsPub[AdType.REWARD_INTERSTITIAL]?.let {
            RewardedInterstitialAd.load(activity.applicationContext, it,
                this.requestAd(), object : RewardedInterstitialAdLoadCallback() {
                    override fun onAdLoaded(ad: RewardedInterstitialAd) {
                        Log.d(TAG, "Ad was loaded.")
                        rewardedInterstitialAd = ad
                        mSuccessLoad?.invoke()
                        this@AdRepositoryFake.setRewardInterstitialFullScreenCallBack()
                    }

                    override fun onAdFailedToLoad(adError: LoadAdError) {
                        Log.d(TAG, adError.toString())
                        rewardedInterstitialAd = null
                        mFailedLoad?.invoke()
                    }
                })
        }
    }

    private fun loadRewardAd() {
        idsPub[AdType.REWARD]?.let {
            RewardedAd.load(activity.applicationContext, it,
                this.requestAd(), object : RewardedAdLoadCallback() {
                    override fun onAdFailedToLoad(adError: LoadAdError) {
                        Log.d(TAG, adError.toString())
                        mRewardedAd = null
                        this@AdRepositoryFake.setRewardFullScreenCallBack()
                        mFailedLoad?.invoke()
                    }

                    override fun onAdLoaded(rewardedAd: RewardedAd) {
                        Log.d(TAG, "Ad was loaded.")
                        mRewardedAd = rewardedAd
                        mSuccessLoad?.invoke()
                    }
            })
        }
    }
    //endregion load ads

    //region callback
    private fun getFullScreenContentCallBack(): FullScreenContentCallback =
        object : FullScreenContentCallback() {
            override fun onAdClicked() {
                // Called when a click is recorded for an ad.
                Log.d(TAG, "Ad was clicked.")
            }

            override fun onAdDismissedFullScreenContent() {
                // Called when ad is dismissed.
                Log.d(TAG, "Ad dismissed fullscreen content.")
                mInterstitialAd = null
            }

            override fun onAdImpression() {
                // Called when an impression is recorded for an ad.
                Log.d(TAG, "Ad recorded an impression.")
            }

            override fun onAdShowedFullScreenContent() {
                // Called when ad is shown.
                Log.d(TAG, "Ad showed fullscreen content.")
            }
        }

    private fun setInterstitialFullScreenCallBack() {
        mInterstitialAd?.fullScreenContentCallback = this.getFullScreenContentCallBack()
    }

    private fun setRewardInterstitialFullScreenCallBack() {
        rewardedInterstitialAd?.fullScreenContentCallback = this.getFullScreenContentCallBack()
    }

    private fun setRewardFullScreenCallBack() {
        mRewardedAd?.fullScreenContentCallback = this.getFullScreenContentCallBack()
    }
    //endregion callback

    //region show ads
    private fun showInterstitialAd() {
        if (mInterstitialAd != null) {
            mInterstitialAd?.show(activity)
            mSuccessWatched?.invoke()
        } else {
            Log.d("TAG", "The interstitial ad wasn't ready yet.")
        }
    }

    private fun showRewardInterstitialAd() {
        if(rewardedInterstitialAd != null) {
            rewardedInterstitialAd?.show(activity) { rewardItem ->
                Log.d(TAG, "User earned reward. ${rewardItem.amount}")
                mSuccessWatched?.invoke()
            }
        }
        else {
            Log.d("TAG", "The rewardedInterstitialAd ad wasn't ready yet.")
        }
    }

    private fun showRewardAd() {
        if (mRewardedAd != null) {
            mRewardedAd?.show(activity) {
                val rewardAmount = it.amount
                val rewardType = it.type
                Log.d(TAG, "User earned the reward. Amount: $rewardAmount - Type: $rewardType")
                mSuccessWatched?.invoke()
            }
        } else {
            Log.d(TAG, "The rewarded ad wasn't ready yet.")
        }
    }
    //region show ads

    /**
     * Realiza a requisição de um novo anúncio na Admob
     * */
    private fun requestAd(): AdRequest = AdRequest.Builder().build()
}