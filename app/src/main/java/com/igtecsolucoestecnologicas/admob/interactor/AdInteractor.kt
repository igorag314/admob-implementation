package com.igtecsolucoestecnologicas.admob.interactor

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.OnUserEarnedRewardListener
import com.igtecsolucoestecnologicas.admob.repository.AdType

interface AdInteractor {
    fun requestBannerAd(): AdRequest
    fun loadAd(
        type: AdType,
        successLoad: (() -> Unit)? = null,
        failedLoad: (() -> Unit)? = null,
        successWatched: (() -> Unit)? = null,
        failedWatched: (() -> Unit)? = null,
    )
    fun showAd()
}