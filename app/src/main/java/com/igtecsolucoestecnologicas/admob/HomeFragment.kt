package com.igtecsolucoestecnologicas.admob

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import com.igtecsolucoestecnologicas.admob.presenter.MainPresenter
import com.igtecsolucoestecnologicas.admob.repository.AdRepositoryFake
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment(), MainPresenter.View {

    private lateinit var mView: View
    private lateinit var presenter: MainPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_home, container, false)

        presenter = MainPresenter(
            this,
            AdRepositoryFake(activity as AppCompatActivity)
        )
        presenter.onLoadPubs()
        presenter.showBanners()

        mView.btnShowInterstitial.setOnClickListener {
            this.presenter.showPub()
            presenter.onLoadPubs()
        }

        return mView
    }

    override fun showBanner(adRequest: AdRequest) {
        mView.advBanner.loadAd(adRequest)
    }

    override fun adWatched() {
        Toast.makeText(context, "Você tem direito a tocar mais 5 músicas", Toast.LENGTH_SHORT).show()
    }
}